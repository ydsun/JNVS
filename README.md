# JNVS

#### 介绍
JNVS即JAVA NETWORK VIDEO SERVER是一个集成于多种协议融合的一个指令服务。目前支持ONVIF,GB28181,RTSP协议接入，流媒体服务基于ZLMediaKit。由于是融合服务，只提供通用基础功能如下：  
1、直播  
2、云台控制（需摄像头支持，RTSP模式不支持云控）  
3、本地录制  
4、本地回放  

#### 软件架构
项目基于JAVA语言实现，框架基于SpringBoot2.X,提供对外访问的Restful接口。


#### 安装&使用

请移步WIKI。

